package com.example.userManagementSystem.demoApp.util;

import com.example.userManagementSystem.demoApp.entitity.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.userManagementSystem.demoApp.config.SecurityConfig.AuthManager.KEY_ROLE;

@Service
public class JwtTokenUtil {

    private final String SECRET_KEY = "mySecretKey";

    public Mono<Claims> extractAllClaims(String token) {
       return Mono.just(Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody());
    }

    public Mono<String> generateToken(User user) {
        return Mono.just(Jwts.builder())
                .flatMap(jwtBuilder -> {
                    Map<String, Object> claims = new HashMap<>();
                    claims.put(KEY_ROLE,user.getRoles());
                    return Mono.just(jwtBuilder
                            .setClaims(claims)
                            .setSubject(user.getUsername())
                            .setIssuedAt(new Date(System.currentTimeMillis()))
                            .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                            .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                            .compact());
                });

    }
    public Mono<Boolean> validateToken(String token) {
        return extractAllClaims(token)
                .map(Claims::getExpiration)
                .map(expiration -> expiration.after(new Date()));
    }


}

