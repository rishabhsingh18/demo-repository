package com.example.userManagementSystem.demoApp.service;

import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.entitity.dto.LoginRequestDto;
import com.example.userManagementSystem.demoApp.entitity.model.User;
import com.example.userManagementSystem.demoApp.repository.UserRepository;
import com.example.userManagementSystem.demoApp.util.JwtTokenUtil;
import com.example.userManagementSystem.demoApp.config.SecurityConfig.LoginManger;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.regex.Pattern;

@Log4j2
@Service
public class AuthService  {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private LoginManger loginManager;

    private final String EMAIL_REGEX = "[A-Za-z0-9+_.-]+@[A-Za-z0-9]+.com$";

    public Mono<UserDto> signUp(UserDto userDto) {
        final boolean newUser = true;
        return Mono.just(userDto)
                .filter(user -> user!=null && Pattern.matches(EMAIL_REGEX,user.getEmail()))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN,"invalid email")))
                .doOnNext(user -> log.info("Started : userRepository.findUserByEmail(String)"))
                .flatMap(user -> userRepository.findUserByEmail(user.getEmail()))
                .doOnNext(user -> log.info("Completed : userRepository.findUserByEmail(String)"))
                .map(user ->!newUser)
                .switchIfEmpty(Mono.just(newUser))
                .filter(user -> user.equals(newUser))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,"User already exist")))
                .map(user -> userDto)
                .map(this::dtoToModel)
                .doOnNext(user -> {
                    log.info("Started : passwordEncoder.encode(String)");
                    user.setPassword(passwordEncoder.encode(user.getPassword()));
                    log.info("Completed : passwordEncoder.encode(String)");
                })
                .flatMap(userRepository::save)
                .map(this::modelToDto);
    }

    public Mono<String> logIn(LoginRequestDto loginRequestDto){
        return Mono.just(loginRequestDto)
                .filter(user -> user!=null && Pattern.matches(EMAIL_REGEX,user.getEmail()))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN,"invalid email")))
                .doOnNext(user -> log.info("Starting : loginManager.authenticate(String, String)"))
                .flatMap(user ->
                        loginManager.authenticate(user.getEmail(), user.getPassword())
                                .doOnNext(authenticatedUser -> log.info("Starting: jwtTokenUtil.generateToken(String)"))
                                .flatMap(authenticatedUser -> jwtTokenUtil.generateToken(authenticatedUser))
                                .doOnNext(authenticatedUser -> log.info("Completed: jwtTokenUtil.generateToken(String)")))
                .doOnNext(user -> log.info("Completed : loginManager.authenticate(String, String)"))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.UNAUTHORIZED,"Token generation failed")));
    }

    public Mono<String> logOut() {
        return Mono.just("asf");
    }

    public User dtoToModel(UserDto userDto){
        return modelMapper.map(userDto,User.class);
    }

    public UserDto modelToDto(User user){
        return modelMapper.map(user,UserDto.class);
    }
}
