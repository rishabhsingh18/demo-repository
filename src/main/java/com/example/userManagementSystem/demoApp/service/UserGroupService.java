package com.example.userManagementSystem.demoApp.service;

import com.example.userManagementSystem.demoApp.entitity.dto.UserGroupDto;
import com.example.userManagementSystem.demoApp.entitity.model.UserGroup;
import com.example.userManagementSystem.demoApp.repository.UserGroupRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

@Service
public class UserGroupService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserGroupRepository userGroupRepository;

    public Mono<UserGroupDto> createGroup(UserGroupDto userGroupDto) {
        return Mono.just(userGroupDto)
                .filter(Objects::nonNull)
                .map(this::dtoToModel)
                .doOnNext(userGroup -> userGroup.addOwnerToGroup(userGroup.getOwnerId()))
                .flatMap(userGroupRepository::save)
                .map(this::modelToDto)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND,"user list empty")));
        }


    public Mono<String> removeGroup(String groupId) {
        return userGroupRepository.deleteById(groupId)
                .then(Mono.just("User with id "+ groupId +" deleted successfully"));
    }

    public Mono<UserGroupDto> addUserInGroup(String groupId, List<String> memberList) {
        return userGroupRepository.findUserGroupByGroupId(groupId)
                .flatMap(Mono::just)
                .doOnNext(userGroup -> userGroup.addMembersToGroup(memberList))
                .flatMap(userGroupRepository::save)
                .map(this::modelToDto);

    }

    public Mono<UserGroupDto> deleteUserFromGroup(String groupId, List<String> memberList) {
        return userGroupRepository.findUserGroupByGroupId(groupId)
                .flatMap(Mono::just)
                .doOnNext(userGroup -> userGroup.deleteMembersFromGroup(memberList))
                .flatMap(userGroupRepository::save)
                .map(this::modelToDto);
    }

    private UserGroup dtoToModel(UserGroupDto usersGroupDto){
        return modelMapper.map(usersGroupDto,UserGroup.class);
    }

    private UserGroupDto modelToDto(UserGroup userGroup){
        return modelMapper.map(userGroup,UserGroupDto.class);
    }
}
