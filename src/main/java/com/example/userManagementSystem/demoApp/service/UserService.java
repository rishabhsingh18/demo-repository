package com.example.userManagementSystem.demoApp.service;

import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.entitity.model.User;
import com.example.userManagementSystem.demoApp.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService  {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    public Flux<UserDto> viewAll(){
        return userRepository
                .findAll()
                .map(this::modelToDto)
                .switchIfEmpty(Mono.error(new RuntimeException()));
    }


    public Mono<UserDto> viewUser(String id){
        return userRepository.findById(id)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND,"user not found")))
                .map(this::modelToDto);
    }

    public Mono<String> deleteUser(String id){
        return userRepository.deleteById(id)
                .then(Mono.just("User with id "+ id +" deleted successfully"));
    }

    public Mono<UserDto> updateUser(String id,UserDto userDto){
       return userRepository.findById(id)
               .flatMap(user -> Mono.just(userDto)
               .map(this::dtoToModel))
               .doOnNext(user -> user.setId(id))
               .flatMap(userRepository::save)
               .map(this::modelToDto);
    }

    private User dtoToModel(UserDto userDto){
        return modelMapper.map(userDto,User.class);
    }

    private UserDto modelToDto(User user){
        return modelMapper.map(user,UserDto.class);
    }


}
