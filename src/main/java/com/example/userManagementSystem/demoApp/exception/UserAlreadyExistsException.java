package com.example.userManagementSystem.demoApp.exception;

public class UserAlreadyExistsException extends RuntimeException{
   public UserAlreadyExistsException(String msg){
       super(msg);
   }
}
