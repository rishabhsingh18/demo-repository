package com.example.userManagementSystem.demoApp.repository;

import com.example.userManagementSystem.demoApp.entitity.model.UserGroup;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserGroupRepository extends ReactiveMongoRepository<UserGroup,String> {
    Mono<UserGroup> findUserGroupByGroupId(String groupId);
}
