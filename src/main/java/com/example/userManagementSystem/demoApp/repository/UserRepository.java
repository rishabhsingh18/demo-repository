package com.example.userManagementSystem.demoApp.repository;

import com.example.userManagementSystem.demoApp.entitity.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User,String> {
    Mono<User> findUserByEmail(String email);
}
