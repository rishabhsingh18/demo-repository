package com.example.userManagementSystem.demoApp.config.SecurityConfig;

import com.example.userManagementSystem.demoApp.repository.UserRepository;
import com.example.userManagementSystem.demoApp.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Component
public class SecurityContextRepository implements ServerSecurityContextRepository {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
        return null;
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange exchange) {
        return Mono.just(exchange.getRequest())
                .map(serverHttpRequest -> serverHttpRequest.getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
                .filter(requestHeader -> requestHeader != null && requestHeader.startsWith("Bearer "))
                .map(requestHeaderValue -> requestHeaderValue.substring(7))
                .flatMap(token -> jwtTokenUtil.validateToken(token)
                        .flatMap(aBoolean -> {
                            if (aBoolean) {
                                return jwtTokenUtil.extractAllClaims(token);
                            }
                            return Mono.error(new ResponseStatusException(HttpStatus.UNAUTHORIZED,"invalid token"));
                        }))
                .map(claims -> new UsernamePasswordAuthenticationToken(
                        claims.getSubject(), null,
                        Collections.singletonList(new SimpleGrantedAuthority("USER"))))
                .map(SecurityContextImpl::new);
    }
}