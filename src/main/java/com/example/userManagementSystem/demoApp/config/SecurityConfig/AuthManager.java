package com.example.userManagementSystem.demoApp.config.SecurityConfig;

import com.example.userManagementSystem.demoApp.util.JwtTokenUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Component
public class AuthManager implements ReactiveAuthenticationManager {

    public static final String KEY_ROLE = "role";

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.just(jwtTokenUtil.validateToken(authentication.getCredentials().toString()))
                .filter(valid -> true)
                .switchIfEmpty(Mono.empty())
                .flatMap(isValid -> jwtTokenUtil.extractAllClaims(authentication.getCredentials().toString()))
                .map(claims -> {
                            return new UsernamePasswordAuthenticationToken(
                                 claims.getSubject(),
                                null,
                                 Collections.singletonList(new SimpleGrantedAuthority(claims.get(KEY_ROLE).toString()))
                    );
                });
    }

}
