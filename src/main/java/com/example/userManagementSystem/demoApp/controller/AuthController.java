package com.example.userManagementSystem.demoApp.controller;

import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.entitity.dto.LoginRequestDto;
import com.example.userManagementSystem.demoApp.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/signup")
    public Mono<UserDto> signUp(@RequestBody UserDto userDto) {
        return authService.signUp(userDto);
    }

    @PostMapping("/login")
    public Mono<String> logIn(@RequestBody LoginRequestDto userLoginDto) {
        return authService.logIn(userLoginDto);
    }

    @PostMapping("/logout")
    public Mono<String> logout() {
        return authService.logOut();
    }

}
