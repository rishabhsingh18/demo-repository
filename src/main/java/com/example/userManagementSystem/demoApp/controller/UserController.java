package com.example.userManagementSystem.demoApp.controller;

import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SecurityRequirement(name = "bearerAuth")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/viewallusers")
    public Flux<UserDto> viewAll(){
        return userService.viewAll();
    }

    @GetMapping("/{id}")
    public Mono<UserDto> view(@PathVariable String id){
        return userService.viewUser(id);
    }

    @PutMapping("/{id}/update")
    public Mono<UserDto> updateUserInfo(@PathVariable String id, @RequestBody UserDto userDto){
        return userService.updateUser(id,userDto);
    }

    @DeleteMapping("/{id}/delete")
    public Mono<String> removeUser(@PathVariable String id){
        return userService.deleteUser(id);
    }
}
