package com.example.userManagementSystem.demoApp.controller;

import com.example.userManagementSystem.demoApp.entitity.dto.UserGroupDto;
import com.example.userManagementSystem.demoApp.service.UserGroupService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@SecurityRequirement(name = "bearerAuth")
@RestController
@RequestMapping("/groups")
public class UserGroupController {

    @Autowired
    private UserGroupService userGroupService;

    @PostMapping("/creategroup")
    public Mono<UserGroupDto> createUserGroup(@RequestBody UserGroupDto userGroupDto){
        return userGroupService.createGroup(userGroupDto);
    }

    @DeleteMapping("/{groupId}/deletegroup")
    public Mono<String> removeUserGroup(@PathVariable String groupId){
        return userGroupService.removeGroup(groupId);
    }

    @PutMapping("/{groupId}/addmember")
    public Mono<UserGroupDto> addUserInGroup(@PathVariable String groupId, @RequestBody List<String> memberList){
        return userGroupService.addUserInGroup(groupId,memberList);
    }

    @PutMapping("/{groupId}/removemember")
    public Mono<UserGroupDto> removeUserFromGroup(@PathVariable String groupId, @RequestBody List<String> memberList){
        return userGroupService.deleteUserFromGroup(groupId,memberList);
    }

}
