package com.example.userManagementSystem.demoApp.entitity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "user_groups")
public class UserGroup implements Serializable{
    @Id
    private String groupId;
    private String groupName;
    private String ownerId;
    private List<String> memberId;

    public void addOwnerToGroup(String memberId) {
        if(this.memberId!=null)
            this.memberId.add(memberId);
    }

    public void addMembersToGroup(List<String> memberId) {
        this.memberId.addAll(memberId);
    }

    public void deleteMembersFromGroup(List<String> memberId) {
        this.memberId.removeAll(memberId);
    }
}
