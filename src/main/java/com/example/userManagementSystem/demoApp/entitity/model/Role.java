package com.example.userManagementSystem.demoApp.entitity.model;

import java.util.List;

public enum Role {
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
    ROLE_USER
}
