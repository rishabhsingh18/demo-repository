package com.example.userManagementSystem.demoApp.entitity.dto;

import com.example.userManagementSystem.demoApp.entitity.model.Address;
import com.example.userManagementSystem.demoApp.entitity.model.Role;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String mobile;
    private Address address;
    private List<Role> roles;
}
