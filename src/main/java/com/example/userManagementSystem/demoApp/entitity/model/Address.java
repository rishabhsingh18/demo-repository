package com.example.userManagementSystem.demoApp.entitity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {
    private String localAddress;
    private String city;
    private String country;
    private String postalCode;
}
