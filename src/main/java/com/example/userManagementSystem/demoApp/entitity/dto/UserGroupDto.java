package com.example.userManagementSystem.demoApp.entitity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupDto {
    private String groupId;
    private String ownerId;
    private String groupName;
    private List<String> memberId;
}
