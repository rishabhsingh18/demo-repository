package com.example.userManagementSystem.demoApp.repository;

import com.example.userManagementSystem.demoApp.entitity.model.Address;
import com.example.userManagementSystem.demoApp.entitity.model.Role;
import com.example.userManagementSystem.demoApp.entitity.model.User;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log4j2
@DataMongoTest
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Order(1)
    public void addUser() {
        log.info("Starting : addUser() ");
        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        Mono<User> savedUser = userRepository.save(user);
        StepVerifier
                .create(savedUser)
                .consumeNextWith(response ->  assertEquals(response,user))
                .verifyComplete();
        log.info("Completed : addUser() ");
    }

    @Test
    @Order(2)
    public void findUserById() {
        log.info("Starting : findUserById() ");
        User user = User.builder()
                .id("abc-def")
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        Mono<User> addUser = userRepository.save(user);
        Mono<User> findByUserId = userRepository.findById("abc-def");

        Mono<User> targetUser = Mono.from(addUser).then(findByUserId);

        StepVerifier
                .create(targetUser)
                .expectNextCount(1)
                .expectComplete()
                .verify();

        log.info("Completed : findUserById() ");
    }

    @Test
    @Order(3)
    public void findUserByEmail() {
        log.info("Starting : findUserByEmail() ");
        User user = User.builder()
                .id("abc-def")
                .firstName("james")
                .lastName("bond")
                .email("jb9@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        Mono<User> addUser = userRepository.save(user);
        Mono<User> findByUserId = userRepository.findUserByEmail("jb9@gmail.com");
        Mono<User> targetUser = Mono.from(addUser).then(findByUserId);

        StepVerifier
                .create(targetUser)
                .consumeNextWith(response ->  assertEquals(response,user))
                .verifyComplete();

        log.info("Completed : findUserByEmail() ");
    }

    @Test
    @Order(4)
    public void findUserByEmailFailed() {
        log.info("Starting : findUserByEmailFailed() ");
        User user = User.builder()
                .id("abc-def")
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        Mono<User> addUser = userRepository.save(user);
        Mono<User> findByUserId = userRepository.findUserByEmail("jb2@gmail.com");

        Mono<User> targetUser = Mono.from(addUser).then(findByUserId);

        StepVerifier
                .create(targetUser)
                .expectNextCount(0)
                .verifyComplete();

        log.info("Completed : findUserByEmailFailed() ");
    }


    @Test
    @Order(5)
    public void removeUser() {
        log.info("Starting : deleteUser() ");
        User user = User.builder()
                .id("abc-def")
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        Mono<User> addUser = userRepository.save(user);
        Mono<Void> deleteUser = userRepository.deleteById("abc-def");
        Mono<User> findByUserId = userRepository.findById("abc-def");

        Mono<User> isDeletedUser = Mono.from(addUser).then(deleteUser).then(findByUserId);

        StepVerifier
                .create(isDeletedUser)
                .expectNextCount(0)
                .expectComplete()
                .verify();

        log.info("Completed : deleteUser() ");
    }
}
