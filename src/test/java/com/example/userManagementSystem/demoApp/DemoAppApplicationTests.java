package com.example.userManagementSystem.demoApp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootTest
class DemoAppApplicationTests {

//	@Test
//	void contextLoads() {
//	}

    @Test
    public void main(){
        DemoAppApplication.main(new String[] {});
    }
}
