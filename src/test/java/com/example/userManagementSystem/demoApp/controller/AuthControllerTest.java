package com.example.userManagementSystem.demoApp.controller;

import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.entitity.model.Address;
import com.example.userManagementSystem.demoApp.entitity.model.Role;
import com.example.userManagementSystem.demoApp.service.AuthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(value = AuthController.class , excludeAutoConfiguration = ReactiveSecurityAutoConfiguration.class)
public class AuthControllerTest {

    @MockBean
    private AuthService authService;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void signupSuccessful() {
        UserDto userDto = UserDto.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("qwerty")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(authService.signUp(userDto)).thenReturn(Mono.just(userDto));

        webTestClient
                .post()
                .uri("/auth/signup")
                .bodyValue(userDto)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .consumeWith(object -> object.getClass().equals(userDto.getClass()));
    }

    @Test
    public void signupUnsuccessful(){
        UserDto userDto = UserDto.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("qwerty")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(authService.signUp(userDto)).thenThrow(new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,"User already exists!"));

        webTestClient
                .post()
                .uri("/auth/signup")
                .bodyValue(userDto)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.NOT_ACCEPTABLE)
                .expectBody()
                .jsonPath("message")
                .isEqualTo("User already exists!");

    }

}
