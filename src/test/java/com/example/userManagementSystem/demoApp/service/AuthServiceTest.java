package com.example.userManagementSystem.demoApp.service;

import com.example.userManagementSystem.demoApp.config.SecurityConfig.LoginManger;
import com.example.userManagementSystem.demoApp.entitity.dto.LoginRequestDto;
import com.example.userManagementSystem.demoApp.entitity.dto.UserDto;
import com.example.userManagementSystem.demoApp.entitity.model.Address;
import com.example.userManagementSystem.demoApp.entitity.model.Role;
import com.example.userManagementSystem.demoApp.entitity.model.User;
import com.example.userManagementSystem.demoApp.repository.UserRepository;
import com.example.userManagementSystem.demoApp.util.JwtTokenUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import reactor.test.StepVerifier;

@Log4j2
@ExtendWith(SpringExtension.class)
@WebFluxTest(value = AuthService.class , excludeAutoConfiguration = ReactiveSecurityAutoConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuthServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private LoginManger loginManger;

    @MockBean
    private  PasswordEncoder passwordEncoder;

    @Autowired
    private AuthService authService;

    @Test
    @Order(1)
    public void SignupSuccessful() {

        log.info("Starting TC1 : SignupSuccessful");
        UserDto userDto = UserDto.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("qwerty")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(userRepository.findUserByEmail(userDto.getEmail())).thenReturn(Mono.empty());
        when(passwordEncoder.encode(userDto.getEmail())).thenReturn("encodedPassword");
        when(userRepository.save(any(User.class))).thenReturn(Mono.just(user));

        Mono<UserDto> monoResult = authService.signUp(userDto);

        StepVerifier
                .create(monoResult)
                .consumeNextWith(newUser -> {
                    Mockito.verify(passwordEncoder).encode(userDto.getPassword());
                    assertNull(newUser.getId());
                    assertEquals(newUser.getEmail(), "jb@gmail.com");
                    assertEquals(newUser.getPassword(), "encodedPassword");
                })
                .verifyComplete();
        log.info("Completed TC1 : SignupSuccessful");
    }

    @Test
    @Order(2)
    public void signupUnsuccessfulInvalidEmail() {
        log.info("Starting TC2 : signupUnsuccessfulInvalidEmail");
        UserDto userDto = UserDto.builder()
                .firstName("james")
                .lastName("bond")
                .email("jbmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(userRepository.findUserByEmail(anyString())).thenReturn(Mono.empty());
        when(userRepository.save(any(User.class))).thenReturn(Mono.just(user));

        Mono<UserDto> monoResult = authService.signUp(userDto);

        StepVerifier
                .create(monoResult)
                .expectErrorMatches(exception -> exception instanceof ResponseStatusException)
                .verify();
        log.info("Completed TC2 : signupUnsuccessfulInvalidEmail");
    }

    @Test
    @Order(3)
    public void signUpUserAlreadyExistException() {
        log.info("Starting TC3 : signUpUserAlreadyExistException");
        UserDto userDto = UserDto.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();


        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(userRepository.findUserByEmail(anyString())).thenReturn(Mono.just(user));
        when(userRepository.save(any(User.class))).thenReturn(Mono.just(user));

        Mono<UserDto> monoResult = authService.signUp(userDto);

        StepVerifier
                .create(monoResult)
                .expectErrorMatches(exception -> exception instanceof ResponseStatusException)
                .verify();
        log.info("Completed TC3 : signUpUserAlreadyExistException");
    }

    @Test
    @Order(4)
    public void loginSuccessful() {
        log.info("Starting TC4 : loginSuccessful");
        LoginRequestDto loginRequestDto = LoginRequestDto.builder()
                .email("jb@gmail.com")
                .password("qwerty")
                .build();

        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(loginManger.authenticate(loginRequestDto.getEmail(), loginRequestDto.getPassword()))
                .thenReturn(Mono.just(user));
        when(jwtTokenUtil.generateToken(user)).thenReturn(Mono.just("tokenValueInString"));
        Mono<String> monoResult = authService.logIn(loginRequestDto);

        StepVerifier
                .create(monoResult)
                .consumeNextWith(responseDto -> {
                    assertEquals(responseDto,"tokenValueInString");
                })
                .verifyComplete();
        log.info("Completed TC4 : loginSuccessful");
    }

    @Test
    @Order(5)
    public void loginUnsuccessfulInvalidEmail() {
        log.info("Starting TC5 : loginUnsuccessfulInvalidEmail");
        LoginRequestDto loginRequestDto = LoginRequestDto.builder()
                .email("jb@gmail.com")
                .password("qwerty")
                .build();

        when(loginManger.authenticate(loginRequestDto.getEmail(),loginRequestDto.getPassword()))
                .thenReturn(Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN,"User not found")));

        Mono<String> monoResult = authService.logIn(loginRequestDto);

        StepVerifier
                .create(monoResult)
                .expectErrorMatches(exception -> exception instanceof ResponseStatusException)
                .verify();
        log.info("Completing TC5 : loginUnsuccessfulInvalidEmail");
    }

    @Test
    @Order(6)
    public void loginUserNotFoundException() {
        log.info("Starting TC6 : loginUserNotFoundException");
        LoginRequestDto loginRequestDto = LoginRequestDto.builder()
                .email("jb@gmail.com")
                .password("qwerty")
                .build();

        when(loginManger.authenticate(loginRequestDto.getEmail(),loginRequestDto.getPassword()))
                        .thenReturn(Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN,"User not found")));

        Mono<String> monoResult = authService.logIn(loginRequestDto);

        StepVerifier
                .create(monoResult)
                .expectErrorMatches(throwable -> throwable instanceof ResponseStatusException)
                .verify();
        log.info("Completed TC6 : loginUserNotFoundException");
    }

    @Test
    @Order(7)
    public void loginFailInvalidPassword() {
        log.info("Starting TC7 : loginFailInvalidPassword");
        LoginRequestDto loginRequestDto = LoginRequestDto.builder()
                .email("jb@gmail.com")
                .password("qwerty")
                .build();

        User user = User.builder()
                .firstName("james")
                .lastName("bond")
                .email("jb@gmail.com")
                .password("encodedPassword")
                .mobile("5634794590")
                .address(Address.builder()
                        .localAddress("65, abc colony,")
                        .city("xyz")
                        .country("US")
                        .postalCode("596534").build())
                .roles(List.of(Role.ROLE_USER))
                .build();

        when(loginManger.authenticate(loginRequestDto.getEmail(),loginRequestDto.getPassword()))
                .thenReturn(Mono.error(new ResponseStatusException(HttpStatus.UNAUTHORIZED,"Invalid password")));

        Mono<String> monoResult = authService.logIn(loginRequestDto);

        StepVerifier
                .create(monoResult)
                .expectErrorMatches(throwable -> throwable instanceof ResponseStatusException)
                .verify();
        log.info("Completed TC7 : loginFailInvalidPassword");
    }
}
